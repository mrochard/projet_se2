#!bin/bash
for version in 7;
do
    #echo "//V$version"  >> ./output.csv;
    #echo "size;1;2;5;10;15;20;25;30;40;50;" >> ./output.csv;
    #for size in 50000000 100000000;
    #do
        #./bin/project -m generation -o ./tmp/test.txt -n $size;
        #echo -n "$size;" >> ./output.csv;
    for split in 50;
    do
        for n in {1..5};
        do
            /usr/bin/time -f "%S;%U" -a -o ./t ./bin/project -m projectV$version -i ./tmp/test.txt -o ./tmp/testsort.txt -k $split;
            rm ./tmp/testsort.txt;
        done;
        total=0;
        for i in $( awk '{ print $0; }' ./t );
        do
            j=$( echo "$i" | awk -F ';' '{ print $1; }');
            k=$( echo "$i" | awk -F ';' '{ print $2; }');
            total=$(echo $total+$j+$k | bc );
        done;
        f=$(echo "scale=2; $total / 5" | bc);
        echo -n "$f;" >> ./output.csv;
        rm ./t;
        echo "$split of $size done";
    done;
       # echo "" >> ./output.csv;
        #rm ./tmp/test.txt
   # done;
done;